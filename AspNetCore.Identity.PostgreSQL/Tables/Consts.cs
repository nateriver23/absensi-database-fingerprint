﻿namespace AspNetCore.Identity.PostgreSQL.Tables
{
    internal static class Consts
    {
        internal static string Schema = "aspnetidentity";
        internal static string ValueNullOrEmpty = "Value cannot be null or empty";
    }
}
