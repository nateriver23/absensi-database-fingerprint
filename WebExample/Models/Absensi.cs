﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebExample.Models
{
    public class Absensi
    {
        [Display(Name = "No.")]
        public int no { get; set; }

        [Display(Name = "ID")]
        public int pin { get; set; }

        [Display(Name = "ID")]
        public string id_user { get; set; }

        [Display(Name = "Nama Pegawai")]
        public string nama { get; set; }

        [Display(Name = "First Name")]
        public string firstname { get; set; }

        [Display(Name = "Last Name")]
        public string lastname { get; set; }

        [Display(Name = "Tanggal Lahir")]
        public string birth_date { get; set; }
        
        [Display(Name = "Alamat")]
        public string address { get; set; }

        [Display(Name = "No. HP")]
        public string hp { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Tanggal")]
        [DataType(DataType.Date)]
        public DateTime tanggal { get; set; }

        [Display(Name = "Tanggal")]
        public string tgl_security { get; set; }

        [Display(Name = "Jam Pulang")]
        public string jam_pulang { get; set; }

        [Display(Name = "Jam Masuk")]
        public string jam_masuk { get; set; }

        [Display(Name = "Terlambat")]
        public string terlambat { get; set; }

        [Display(Name = "Status Lembur")]
        public string lembur { get; set; }

        [Display(Name = "Tanggal Mulai")]
        [DataType(DataType.Date)]
        public DateTime start_date { get; set; }

        [Display(Name = "Tanggal Selesai")]
        [DataType(DataType.Date)]
        public DateTime end_date { get; set; }

        [Display(Name = "Jumlah Lembur")]
        public int count_lembur { get; set; }

        [Display(Name = "Jumlah Terlambat")]
        public int count_terlambat { get; set; }

        [Display(Name = "Total Lembur (menit)")]
        public int sum_lembur { get; set; }

        [Display(Name = "Total Terlambat (menit)")]
        public int sum_terlambat { get; set; }

        [Display(Name = "Jumlah Hari Masuk")]
        public int count_masuk { get; set; }

        [Display(Name = "Pulang Cepat")]
        public int pulang_cepat { get; set; }

        [Display(Name = "Libur")]
        public int libur { get; set; }
    }
}
