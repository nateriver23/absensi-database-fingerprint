﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace WebExample.Models
{
    public class AbsensiContext
    {
        public string ConnectionString { get; set; }

        public AbsensiContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Absensi> GetDetails()
        {
            List<Absensi> list = new List<Absensi>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select b.pin, b.alias from emp as b where b.emp_status = 0 order by b.first_name asc", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    int nomor = 1;
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            no = nomor,

                            pin = Convert.ToInt32(reader["pin"]),
                            nama = reader["alias"].ToString()
                        });
                        nomor++;
                    }
                }
            }
            return list;
        }

        public List<Absensi> GetReportDetail(string ID, string begin_date, string end_date)
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd1 = new MySqlCommand(String.Format("select a.pin, a.alias, a.birth_date, a.address, a.phone, c.email, b.start_date from emp as a " +
                    "left join security_schedule as b on a.pin = b.pin " +
                    "left join emp_email as c on a.pin = c.pin " +
                    "where a.pin = {0}", ID), conn);
                string nama = "";
                string start_date = "";
                using (var reader1 = cmd1.ExecuteReader())
                {
                    while (reader1.Read())
                    {
                        nama = reader1["alias"].ToString();
                        start_date = reader1["start_date"].ToString();
                        

                    }
                }
                conn.Close();
                conn.Open();
                if (String.IsNullOrEmpty(start_date))
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("select b.alias, a.pin, date(a.scan_date) as tanggal, " +
                       "group_concat(distinct convert(time(a.scan_date), char(8)) order by time(a.scan_date) asc) as waktu, " +
                       "convert((((cin - (cin % 60)) / 60) * 10000) + ((cin % 60) * 100), time) as masuk, " +
                       "convert((((ecin - (ecin % 60)) / 60) * 10000) + ((ecin % 60) * 100), time) as akhir_masuk " +
                       "from att_log as a " +
                       "inner join emp as b on a.pin = b.pin " +
                       "inner join emp_schedule as c on b.emp_id_auto = c.emp_id_auto " +
                       "inner join schedule as d on c.schedule_id = d.schedule_id " +
                       "inner join schedule_detail as e on d.schedule_id = e.schedule_id " +
                       "inner join shift as f on e.shift_id = f.shift_id where a.pin = {0} " +
                       "and date(a.scan_date) between '{1}' and '{2}'" +
                       "group by a.pin, tanggal order by tanggal desc", ID, begin_date, end_date), conn);


                    using (var reader = cmd.ExecuteReader())
                    {
                        int nomor = 1;
                        while (reader.Read())
                        {
                            string jam_msk = "";
                            string jam_plg = "";
                            string telat = "";
                            string lembur = "";
                            TimeSpan late;
                            TimeSpan ot;
                            string hari = DateTime.Parse(reader["tanggal"].ToString()).ToString("ddd");

                            if (reader["waktu"].ToString().Length > 8)
                            {
                                string s_jam_msk = reader["waktu"].ToString().Substring(0, 8);
                                string s_jam_plg = reader["waktu"].ToString().Substring(reader["waktu"].ToString().Length - 8, 8);

                                if (hari == "Sat" || hari == "Mon")
                                {
                                    jam_msk = s_jam_msk;
                                    jam_plg = s_jam_plg;
                                    ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) - DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null);
                                    telat = "Tidak Terlambat";
                                }
                                else if (DateTime.ParseExact(s_jam_msk, "HH:mm:ss", null) <= DateTime.ParseExact(reader["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                {
                                    jam_msk = s_jam_msk;

                                    if (DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                    else
                                    {
                                        telat = "Tidak Terlambat";
                                    }
                                }

                                if (DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) > DateTime.ParseExact(reader["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                {
                                    jam_plg = s_jam_plg;

                                    if (DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) > DateTime.ParseExact("20:00:00", "HH:mm:ss", null))
                                    {
                                        ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) - DateTime.ParseExact("20:00:00", "HH:mm:ss", null);
                                        lembur = ot.ToString();
                                    }
                                }


                            }
                            else
                            {
                                if (DateTime.ParseExact(reader["waktu"].ToString(), "HH:mm:ss", null) > DateTime.ParseExact("13:00:00", "HH:mm:ss", null))
                                {
                                    jam_plg = reader["waktu"].ToString().Substring(0, 8);

                                    if (hari == "Sat" || hari == "Sun")
                                    {
                                        ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null).AddHours(-1) - DateTime.ParseExact("08:00:00", "HH:mm:ss", null);
                                        ot.ToString();
                                    }
                                    else if (DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) > DateTime.ParseExact("20:00:00", "HH:mm:ss", null))
                                    {
                                        ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) - DateTime.ParseExact("20:00:00", "HH:mm:ss", null);
                                        lembur = ot.ToString();
                                    }
                                }
                                else
                                {
                                    jam_msk = reader["waktu"].ToString().Substring(0, 8);

                                    if (hari == "Sat" || hari == "Sun")
                                    {
                                        ot = DateTime.ParseExact("23:59:59".ToString(), "HH:mm:ss", null).AddHours(-1) - DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null);
                                        lembur = ot.ToString();
                                    }
                                    else if (DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                    else
                                    {
                                        telat = "Tidak Terlambat";
                                    }
                                }
                            }



                            list.Add(new Absensi()
                            {
                                no = nomor,
                                nama = reader["alias"].ToString(),
                                tanggal = Convert.ToDateTime(reader["tanggal"]).Date,
                                jam_masuk = jam_msk,
                                terlambat = telat,
                                jam_pulang = jam_plg,
                                lembur = lembur
                            });
                        }
                    }
                    return list;
                }
                else
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("select a.pin, c.alias, datediff(date(a.scan_date), b.start_date) as date_different, " +
                                "floor(datediff(date(a.scan_date), b.start_date)/ 2) as shiftno, weekday(a.scan_date), " +
                                "convert((((cin - (cin % 60)) / 60) * 10000) + ((cin % 60) * 100), time) as masuk, " +
                                "convert((((cout - (cout % 60)) / 60) * 10000) + ((cout % 60) * 100), time) as pulang," +
                                "group_concat(distinct convert(a.scan_date, char(20)) order by scan_date asc) as waktu_masuk from att_log as a " +
                                "inner join security_schedule as b on a.pin = b.pin " +
                                "inner join emp as c on a.pin = c.pin " +
                                "inner join emp_schedule as d on c.emp_id_auto = d.emp_id_auto " +
                                "inner join schedule as e on d.schedule_id = e.schedule_id " +
                                "inner join schedule_detail as f on e.schedule_id = f.schedule_id " +
                                "inner join shift as g on f.shift_id = g.shift_id " +
                                "where a.pin = '{0}' " +
                                "and date(a.scan_date) between '{1}' and '{2}'" +
                                "group by shiftno", ID, begin_date, end_date), conn);


                    using (var reader = cmd.ExecuteReader())
                    {
                        int nomor = 1;
                        while (reader.Read())
                        {
                            string[] waktu_msk = reader["waktu_masuk"].ToString().Split(",");
                            string msk = waktu_msk[0];
                            string plg = waktu_msk[waktu_msk.Length - 1];
                            string[] str_msk = waktu_msk[0].Split(" ");
                            string[] str_plg = waktu_msk[waktu_msk.Length - 1].Split(" ");
                            DateTime tgl_msk = DateTime.Parse(msk);
                            DateTime tgl_plg = DateTime.Parse(plg);
                            string hari_msk = tgl_msk.ToString("ddd");
                            string hari_plg = tgl_plg.ToString("ddd");
                            TimeSpan ot;
                            TimeSpan late;
                            string str_tgl_msk = "";
                            string str_tgl_plg = "";
                            string lembur = "";
                            string telat = "";
                            string jam_msk = "";
                            string jam_plg = "";

                            if (msk == plg)
                            {
                                str_tgl_msk = tgl_msk.ToString("MM/dd/yyyy");
                                jam_msk = str_msk[1];
                                str_tgl_plg = "Tidak Scan Pulang";
                                if (hari_msk == "Sat" || hari_msk == "Sun")
                                {
                                    ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                    lembur = ot.ToString();
                                }
                                else
                                {
                                    if (DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                }
                            }
                            else
                            {
                                jam_msk = str_msk[1];
                                jam_plg = str_plg[1];
                                str_tgl_msk = tgl_msk.ToString("MM/dd/yyyy");
                                str_tgl_plg = tgl_plg.ToString("MM/dd/yyyy");

                                if (hari_msk == "Sat" && hari_plg == "Sun")
                                {
                                    ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                    ot = ot + (DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact("00:00:00", "HH:mm:ss", null));
                                    lembur = ot.ToString();
                                }
                                else if (hari_msk == "Sun")
                                {
                                    ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                    lembur = ot.ToString();
                                }
                                else if (hari_plg == "Sat")
                                {
                                    ot = DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact("00:00:00", "HH:mm:ss", null);
                                    lembur = ot.ToString();
                                }
                                else
                                {
                                    if (DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                    else
                                    {
                                        telat = "Tidak Terlambat";
                                    }
                                }
                            }



                            list.Add(new Absensi()
                            {
                                no = nomor,
                                nama = reader["alias"].ToString(),
                                tanggal = Convert.ToDateTime(str_tgl_msk),
                                jam_masuk = jam_msk,
                                terlambat = telat,
                                jam_pulang = jam_plg,
                                lembur = lembur
                            });
                            nomor++;
                        }
                    }
                    return list;
                }
            }
        }

        public List<Absensi> GetIdByEmail(string email)
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(String.Format("select pin from emp_email where email = '{0}\r'", email.ToLower()), conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!String.IsNullOrEmpty(reader["pin"].ToString())){
                            list.Add(new Absensi() { id_user = reader["pin"].ToString() });
                        }
                    }
                }
            }
            return list;
        }

        public string GetPinByEmail(string email)
        {
            
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(String.Format("select pin from emp_email where email = '{0}\r'", email.ToLower()), conn);
                using (var reader = cmd.ExecuteReader())
                {
                    string pin = "";
                    while (reader.Read())
                    {
                        if (!String.IsNullOrEmpty(reader["pin"].ToString()))
                        {
                            pin = reader["pin"].ToString();
                        }
                    }
                    return pin;
                }
            }   
        }

        public List<Absensi> GetDetailAbsen(string ID)
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd1 = new MySqlCommand(String.Format("select a.pin, a.alias, a.birth_date, a.address, a.phone, c.email, b.start_date from emp as a " +
                    "left join security_schedule as b on a.pin = b.pin " +
                    "left join emp_email as c on a.pin = c.pin " +
                    "where a.pin = {0}", ID), conn);
                int pin = 0;
                string nama = "";
                string birth_date = "";
                string address = "";
                string phone = "";
                string email = "";
                string start_date = "";
                using (var reader1 = cmd1.ExecuteReader())
                {
                    while (reader1.Read())
                    {
                        pin = Convert.ToInt32(reader1["pin"]);
                        nama = reader1["alias"].ToString();
                        start_date = reader1["start_date"].ToString();
                        birth_date = Convert.ToDateTime(reader1["birth_date"].ToString()).ToString("dd MM yyyy");
                        address = reader1["address"].ToString();
                        phone = reader1["phone"].ToString();
                        email = reader1["email"].ToString();
                    }
                }
                conn.Close();
                conn.Open();
                if (String.IsNullOrEmpty(start_date))
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("select b.alias, a.pin, date(a.scan_date) as tanggal, " +
                        "group_concat(distinct convert(time(a.scan_date), char(8)) order by time(a.scan_date) asc) as waktu, " +
                        "convert((((cin - (cin % 60)) / 60) * 10000) + ((cin % 60) * 100), time) as masuk, " +
                        "convert((((ecin - (ecin % 60)) / 60) * 10000) + ((ecin % 60) * 100), time) as akhir_masuk " +
                        "from att_log as a " +
                        "inner join emp as b on a.pin = b.pin " +
                        "inner join emp_schedule as c on b.emp_id_auto = c.emp_id_auto " +
                        "inner join schedule as d on c.schedule_id = d.schedule_id " +
                        "inner join schedule_detail as e on d.schedule_id = e.schedule_id " +
                        "inner join shift as f on e.shift_id = f.shift_id where a.pin = {0} group by a.pin, tanggal order by tanggal desc", ID), conn);
                    using (var reader = cmd.ExecuteReader())
                    {
                        int nomor = 1;
                        while (reader.Read())
                        {
                            string jam_msk = "";
                            string jam_plg = "";
                            string telat = "";
                            string lembur = "";
                            TimeSpan late;
                            TimeSpan ot;
                            string hari = DateTime.Parse(reader["tanggal"].ToString()).ToString("ddd");

                            if (reader["waktu"].ToString().Length > 8)
                            {
                                string s_jam_msk = reader["waktu"].ToString().Substring(0, 8);
                                string s_jam_plg = reader["waktu"].ToString().Substring(reader["waktu"].ToString().Length - 8, 8);

                                if (hari == "Sat" || hari == "Mon")
                                {
                                    jam_msk = s_jam_msk;
                                    jam_plg = s_jam_plg;
                                    ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) - DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null);
                                    telat = "Tidak Terlambat";
                                }
                                else if (DateTime.ParseExact(s_jam_msk, "HH:mm:ss", null) <= DateTime.ParseExact(reader["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                {
                                    jam_msk = s_jam_msk;

                                    if (DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                    else
                                    {
                                        telat = "Tidak Terlambat";
                                    }
                                }

                                if (DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) > DateTime.ParseExact(reader["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                {
                                    jam_plg = s_jam_plg;

                                    if (DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) > DateTime.ParseExact("20:00:00", "HH:mm:ss", null))
                                    {
                                        ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) - DateTime.ParseExact("20:00:00", "HH:mm:ss", null);
                                        lembur = ot.ToString();
                                    }
                                }


                            }
                            else
                            {
                                if (DateTime.ParseExact(reader["waktu"].ToString(), "HH:mm:ss", null) > DateTime.ParseExact("13:00:00", "HH:mm:ss", null))
                                {
                                    jam_plg = reader["waktu"].ToString().Substring(0, 8);

                                    if (hari == "Sat" || hari == "Sun")
                                    {
                                        ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null).AddHours(-1) - DateTime.ParseExact("08:00:00", "HH:mm:ss", null);
                                        ot.ToString();
                                    }
                                    else if (DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) > DateTime.ParseExact("20:00:00", "HH:mm:ss", null))
                                    {
                                        ot = DateTime.ParseExact(jam_plg.ToString(), "HH:mm:ss", null) - DateTime.ParseExact("20:00:00", "HH:mm:ss", null);
                                        lembur = ot.ToString();
                                    }
                                }
                                else
                                {
                                    jam_msk = reader["waktu"].ToString().Substring(0, 8);

                                    if (hari == "Sat" || hari == "Sun")
                                    {
                                        ot = DateTime.ParseExact("23:59:59".ToString(), "HH:mm:ss", null).AddHours(-1) - DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null);
                                        lembur = ot.ToString();
                                    }
                                    else if (DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(jam_msk.ToString(), "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                    else
                                    {
                                        telat = "Tidak Terlambat";
                                    }
                                }
                            }



                            list.Add(new Absensi()
                            {
                                no = nomor,
                                nama = reader["alias"].ToString(),
                                birth_date = birth_date,
                                address = address,
                                hp = phone,
                                email = email,
                                tanggal = Convert.ToDateTime(reader["tanggal"]).Date,
                                jam_masuk = jam_msk,
                                terlambat = telat,
                                jam_pulang = jam_plg,
                                lembur = lembur
                            });
                        }
                    }
                    return list;
                }
                else
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("select a.pin, c.alias, datediff(date(a.scan_date), b.start_date) as date_different, " +
                                "floor(datediff(date(a.scan_date), b.start_date)/ 2) as shiftno, weekday(a.scan_date), " +
                                "convert((((cin - (cin % 60)) / 60) * 10000) + ((cin % 60) * 100), time) as masuk, " +
                                "convert((((cout - (cout % 60)) / 60) * 10000) + ((cout % 60) * 100), time) as pulang," +
                                "group_concat(distinct convert(a.scan_date, char(20)) order by scan_date asc) as waktu_masuk from att_log as a " +
                                "inner join security_schedule as b on a.pin = b.pin " +
                                "inner join emp as c on a.pin = c.pin " +
                                "inner join emp_schedule as d on c.emp_id_auto = d.emp_id_auto " +
                                "inner join schedule as e on d.schedule_id = e.schedule_id " +
                                "inner join schedule_detail as f on e.schedule_id = f.schedule_id " +
                                "inner join shift as g on f.shift_id = g.shift_id " +
                                "where a.pin = '{0}' group by shiftno", ID), conn);
                    using (var reader = cmd.ExecuteReader())
                    {
                        int nomor = 1;
                        while (reader.Read())
                        {
                            string[] waktu_msk = reader["waktu_masuk"].ToString().Split(",");
                            string msk = waktu_msk[0];
                            string plg = waktu_msk[waktu_msk.Length - 1];
                            string[] str_msk = waktu_msk[0].Split(" ");
                            string[] str_plg = waktu_msk[waktu_msk.Length - 1].Split(" ");
                            DateTime tgl_msk = DateTime.Parse(msk);
                            DateTime tgl_plg = DateTime.Parse(plg);
                            string hari_msk = tgl_msk.ToString("ddd");
                            string hari_plg = tgl_plg.ToString("ddd");
                            TimeSpan ot;
                            TimeSpan late;
                            string str_tgl_msk = "";
                            string str_tgl_plg = "";
                            string lembur = "";
                            string telat = "";
                            string jam_msk = "";
                            string jam_plg = "";

                            if (msk == plg)
                            {
                                str_tgl_msk = tgl_msk.ToString("MM/dd/yyyy");
                                jam_msk = str_msk[1];
                                str_tgl_plg = "Tidak Scan Pulang";
                                if (hari_msk == "Sat" || hari_msk == "Sun")
                                {
                                    ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                    lembur = ot.ToString();
                                }
                                else
                                {
                                    if (DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                }
                            }
                            else
                            {
                                jam_msk = str_msk[1];
                                jam_plg = str_plg[1];
                                str_tgl_msk = tgl_msk.ToString("MM/dd/yyyy");
                                str_tgl_plg = tgl_plg.ToString("MM/dd/yyyy");

                                if (hari_msk == "Sat" && hari_plg == "Sun")
                                {
                                    ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                    ot = ot + (DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact("00:00:00", "HH:mm:ss", null));
                                    lembur = ot.ToString();
                                }
                                else if (hari_msk == "Sun")
                                {
                                    ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                    lembur = ot.ToString();
                                }
                                else if (hari_plg == "Sat")
                                {
                                    ot = DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact("00:00:00", "HH:mm:ss", null);
                                    lembur = ot.ToString();
                                }
                                else
                                {
                                    if (DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                                    {
                                        late = DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                                        telat = late.ToString();
                                    }
                                    else
                                    {
                                        telat = "Tidak Terlambat";
                                    }
                                }
                            }



                            list.Add(new Absensi()
                            {
                                no = nomor,
                                nama = reader["alias"].ToString(),
                                birth_date = birth_date,
                                address = address,
                                hp = phone,
                                email = email,
                                tanggal = Convert.ToDateTime(str_tgl_msk),
                                jam_masuk = jam_msk,
                                terlambat = telat,
                                jam_pulang = jam_plg,
                                lembur = lembur
                            });
                            nomor++;
                        }
                    }
                    return list;
                }
            }
        }

        public List<Absensi> GetTodayAbsen()
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select a.pin, a.alias, time(b.scan_date) as waktu, convert((((f.cin-(f.cin%60))/60)*10000)+((f.cin%60)*100), time) as masuk from emp as a inner join att_log as b on b.pin = a.pin inner join emp_schedule as c on a.emp_id_auto = c.emp_id_auto inner join schedule as d on c.schedule_id = d.schedule_id inner join schedule_detail as e on d.schedule_id = e.schedule_id inner join shift as f on e.shift_id = f.shift_id where date(b.scan_date) = curdate() group by a.pin order by a.first_name", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    string telat;
                    TimeSpan late;

                    int nomor = 1;
                    while (reader.Read())
                    {
                        if (DateTime.ParseExact(reader["waktu"].ToString(), "HH:mm:ss", null) > DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null))
                        {
                            late = DateTime.ParseExact(reader["waktu"].ToString(), "HH:mm:ss", null) - DateTime.ParseExact(reader["masuk"].ToString(), "HH:mm:ss", null);
                            telat = late.ToString();
                        }
                        else
                        {
                            telat = "Tidak Terlambat";
                        }
                        list.Add(new Absensi()
                        {
                            no = nomor,
                            id_user = Convert.ToString(reader["pin"]),
                            nama = reader["alias"].ToString(),
                            jam_masuk = reader["waktu"].ToString(),
                            terlambat = telat
                        });
                        nomor++;
                    }
                }
            }
            return list;
        }

        public List<Absensi> GetTodayAbsent()
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select a.pin, a.alias from emp as a where a.emp_status = 0 and not exists(select * from att_log as b where b.pin = a.pin and date(b.scan_date) = curdate()) order by a.first_name", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    int nomor = 1;
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            no = nomor,
                            id_user = Convert.ToString(reader["pin"]),
                            nama = reader["alias"].ToString()
                        });
                        nomor++;
                    }

                }
            }

            return list;
        }

        //report rekapitulasi absensi
        public List<Absensi> GetRekapPeriode(string start_date, string end_date)
        {
            List<Absensi> list = new List<Absensi>();
            if (!String.IsNullOrEmpty(start_date) && !String.IsNullOrEmpty(end_date))
            {

                using (MySqlConnection conn = GetConnection())
                {
                    int nomor = 0;
                    string[] str_pin = new string[1000];
                    string[] nama_karyawan = new string[1000];
                    string[] tgl_mulai = new string[1000];
                    int count_in = 0;
                    int count_late = 0;
                    int sum_late = 0;
                    int count_ot = 0;
                    int sum_ot = 0;
                    int home_fast = 0;
                    string jam_msk;
                    string jam_plg;
                    conn.Open();
                    MySqlCommand cmd1 = new MySqlCommand("select a.pin, b.start_date as tgl_mulai, a.alias, c.func_name as jabatan from emp as a inner join func as c on a.func_id_auto = c.func_id_auto left join security_schedule as b on a.pin = b.pin where a.emp_status = 0 order by a.alias asc", conn);
                    using (var reader1 = cmd1.ExecuteReader())
                    {

                        while (reader1.Read())
                        {
                            str_pin[nomor] = reader1["pin"].ToString();
                            nama_karyawan[nomor] = reader1["alias"].ToString();
                            tgl_mulai[nomor] = reader1["tgl_mulai"].ToString();
                            nomor++;
                        }
                    }

                    for (int a = 0; a <= nomor; a++)
                    {
                        conn.Close();
                        conn.Open();
                        count_in = 0;
                        count_late = 0;
                        sum_late = 0;
                        count_ot = 0;
                        sum_ot = 0;
                        home_fast = 0;
                        if (!String.IsNullOrEmpty(tgl_mulai[a]))
                        {
                            string s_date;
                            string f_date;
                            TimeSpan datediff_masuk = DateTime.Parse(start_date) - DateTime.Parse(tgl_mulai[a]);
                            TimeSpan datediff_pulang = DateTime.Parse(end_date) - DateTime.Parse(tgl_mulai[a]);
                            if (Convert.ToInt32(datediff_masuk.TotalDays) % 2 == 0)
                            {
                                s_date = start_date;
                            }
                            else
                            {
                                s_date = DateTime.ParseExact(start_date, "yyyy-MM-dd", null).AddDays(-1).ToString();
                            }

                            if (Convert.ToInt32(datediff_pulang.TotalDays) % 2 == 0)
                            {
                                f_date = end_date;
                            }
                            else
                            {
                                f_date = DateTime.ParseExact(end_date, "yyyy-MM-dd", null).AddDays(-1).ToString();
                            }
                            MySqlCommand cmd2 = new MySqlCommand(String.Format("select a.pin, datediff(date(a.scan_date), b.start_date) as date_different, " +
                                "floor(datediff(date(a.scan_date), b.start_date)/ 2) as shiftno, weekday(a.scan_date), " +
                                "convert((((cin - (cin % 60)) / 60) * 10000) + ((cin % 60) * 100), time) as masuk, " +
                                "convert((((cout - (cout % 60)) / 60) * 10000) + ((cout % 60) * 100), time) as pulang," +
                                "group_concat(distinct convert(a.scan_date, char(20)) order by scan_date asc) as waktu_masuk from att_log as a " +
                                "inner join security_schedule as b on a.pin = b.pin " +
                                "inner join emp as c on a.pin = c.pin " +
                                "inner join emp_schedule as d on c.emp_id_auto = d.emp_id_auto " +
                                "inner join schedule as e on d.schedule_id = e.schedule_id " +
                                "inner join schedule_detail as f on e.schedule_id = f.schedule_id " +
                                "inner join shift as g on f.shift_id = g.shift_id " +
                                "where a.pin = '{0}' and a.scan_date between '{1}' and '{2}' group by shiftno", str_pin[a], start_date, end_date), conn);
                            using (var reader2 = cmd2.ExecuteReader())
                            {
                                while (reader2.Read())
                                {
                                    string[] waktu_msk = reader2["waktu_masuk"].ToString().Split(",");
                                    string msk = waktu_msk[0];
                                    string plg = waktu_msk[waktu_msk.Length - 1];
                                    string[] str_msk = waktu_msk[0].Split(" ");
                                    string[] str_plg = waktu_msk[waktu_msk.Length - 1].Split(" ");
                                    DateTime tgl_msk = DateTime.Parse(msk);
                                    DateTime tgl_plg = DateTime.Parse(plg);
                                    string hari_msk = tgl_msk.ToString("ddd");
                                    string hari_plg = tgl_plg.ToString("ddd");
                                    TimeSpan ot;
                                    TimeSpan late;

                                    if (hari_msk == "Sat" || hari_msk == "Sun")
                                    {
                                        ot = DateTime.ParseExact("23:59:59", "HH:mm:ss", null) - DateTime.ParseExact(str_msk[1], "HH:mm:ss", null);
                                        sum_ot = sum_ot + Convert.ToInt32(ot.TotalMinutes);
                                        count_ot++;

                                        if (hari_plg == "Sat" || hari_plg == "Sun")
                                        {
                                            ot = DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact("00:00:00", "HH:mm:ss", null);
                                            sum_ot = sum_ot + Convert.ToInt32(ot.TotalMinutes);
                                        }
                                    }
                                    else if (hari_plg == "Sat" || hari_plg == "Sun")
                                    {
                                        ot = DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact("00:00:00", "HH:mm:ss", null);
                                        sum_ot = sum_ot + Convert.ToInt32(ot.TotalMinutes);
                                        count_ot++;
                                    }
                                    else
                                    {
                                        if (DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) > DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null))
                                        {
                                            late = DateTime.ParseExact(str_msk[1], "HH:mm:ss", null) - DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null);
                                            sum_late = sum_late + Convert.ToInt32(late.TotalMinutes);
                                            count_late++;
                                        }

                                        if (DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) < DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null))
                                        {
                                            home_fast++;
                                        }
                                        //else if (DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) > DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null))
                                        //{
                                        //ot = DateTime.ParseExact(str_plg[1], "HH:mm:ss", null) - DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null);
                                        //sum_ot = sum_ot + Convert.ToInt32(ot.TotalMinutes);
                                        //count_ot++;
                                        //}
                                    }
                                    count_in++;
                                }
                            }
                        }
                        else
                        {
                            MySqlCommand cmd2 = new MySqlCommand(String.Format("select f.kode, b.alias, a.pin, date(a.scan_date) as tanggal, " +
                                "group_concat(distinct convert(time(a.scan_date), char(8)) order by time(a.scan_date) asc) as waktu, " +
                                "convert((((cin - (cin % 60)) / 60) * 10000) + ((cin % 60) * 100), time) as masuk, " +
                                "convert((((ecin - (ecin % 60)) / 60) * 10000) + ((ecin % 60) * 100), time) as akhir_masuk, " +
                                "convert((((cout - (cout % 60)) / 60) * 10000) + ((cout % 60) * 100), time) as pulang from att_log as a " +
                                "inner join emp as b on a.pin = b.pin " +
                                "inner join emp_schedule as c on b.emp_id_auto = c.emp_id_auto " +
                                "inner join schedule as d on c.schedule_id = d.schedule_id " +
                                "inner join schedule_detail as e on d.schedule_id = e.schedule_id " +
                                "inner join shift as f on e.shift_id = f.shift_id " +
                                "where a.pin = '{0}' and date(a.scan_date) between '{1}' and '{2}' group by a.pin, tanggal order by tanggal desc", str_pin[a], start_date, end_date), conn);
                            using (var reader2 = cmd2.ExecuteReader())
                            {

                                while (reader2.Read())
                                {
                                    string s_jam_msk = reader2["waktu"].ToString().Substring(0, 8);
                                    string s_jam_plg = reader2["waktu"].ToString().Substring(reader2["waktu"].ToString().Length - 8, 8);
                                    string[] str_tanggal = reader2["tanggal"].ToString().Split(" ");
                                    DateTime tanggal = Convert.ToDateTime(str_tanggal[0]).Date;
                                    string hari = tanggal.ToString("ddd");
                                    TimeSpan ot;
                                    TimeSpan late;

                                    if (hari == "Sat" || hari == "Sun")
                                    {
                                        if (reader2["waktu"].ToString().Length > 8)
                                        {
                                            if (DateTime.ParseExact(s_jam_msk, "HH:mm:ss", null) <= DateTime.ParseExact(reader2["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                            {
                                                jam_msk = s_jam_msk;
                                                if (DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) > DateTime.ParseExact(reader2["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                                {
                                                    jam_plg = s_jam_plg;
                                                    ot = DateTime.ParseExact(jam_plg, "HH:mm:ss", null) - DateTime.ParseExact(jam_msk, "HH:mm:ss", null);
                                                    sum_ot = sum_ot + Convert.ToInt32(ot.TotalMinutes);
                                                }
                                                else
                                                {
                                                    sum_ot = sum_ot + 480;
                                                }
                                            }
                                            else
                                            {
                                                sum_ot = sum_ot + 480;
                                            }
                                        }
                                        else
                                        {
                                            sum_ot = sum_ot + 480;
                                        }
                                        count_ot++;
                                        count_in++;

                                    }
                                    else
                                    {
                                        if (DateTime.ParseExact(s_jam_msk, "HH:mm:ss", null) <= DateTime.ParseExact(reader2["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                        {
                                            if (DateTime.ParseExact(s_jam_msk, "HH:mm:ss", null) >= DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null))
                                            {
                                                late = DateTime.ParseExact(s_jam_msk, "HH:mm:ss", null) - DateTime.ParseExact(reader2["masuk"].ToString(), "HH:mm:ss", null);
                                                sum_late = sum_late + Convert.ToInt32(late.TotalMinutes);
                                                count_late++;
                                            }

                                        }

                                        if (DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) > DateTime.ParseExact(reader2["akhir_masuk"].ToString(), "HH:mm:ss", null))
                                        {
                                            if (DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) >= DateTime.ParseExact("20:00:00", "HH:mm:ss", null))
                                            {
                                                ot = DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) - DateTime.ParseExact("20:00:00", "HH:mm:ss", null);
                                                sum_ot = sum_ot + Convert.ToInt32(ot.TotalMinutes);
                                                count_ot++;
                                            }

                                            if (DateTime.ParseExact(s_jam_plg, "HH:mm:ss", null) < DateTime.ParseExact(reader2["pulang"].ToString(), "HH:mm:ss", null))
                                            {
                                                home_fast++;
                                            }
                                        }
                                        count_in++;
                                    }
                                }

                            }
                        }
                        list.Add(new Absensi()
                        {
                            pin = Convert.ToInt32(str_pin[a]),
                            start_date = Convert.ToDateTime(start_date),
                            end_date = Convert.ToDateTime(end_date),
                            nama = nama_karyawan[a],
                            count_masuk = count_in,
                            count_terlambat = count_late,
                            count_lembur = count_ot,
                            sum_lembur = sum_ot,
                            sum_terlambat = sum_late,
                            pulang_cepat = home_fast

                        });
                    }


                }
            }
            return list;
        }

        public List<Absensi> GetSchedule()
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select a.pin, a.start_date, b.alias from security_schedule as a inner join emp as b on a.pin = b.pin order by b.alias asc", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            pin = Convert.ToInt32(reader["pin"]),
                            nama = reader["alias"].ToString(),
                            start_date = Convert.ToDateTime(reader["start_date"])
                        });
                    }
                }
            }
            return list;
        }

        public List<Absensi> CreateSchedule(string pin, string start_date)
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(String.Format("insert into security_schedule values ('{0}','{1}')", pin, start_date), conn);
                using(var reader1 = cmd.ExecuteReader())
                {
                    while (reader1.Read())
                    {
                    }
                }
                conn.Close();
                conn.Open();
                MySqlCommand cmd1 = new MySqlCommand("select a.pin, a.alias from emp as a inner join func as c on a.func_id_auto = c.func_id_auto where c.func_name = 'Security' and a.emp_status = 0 and not exists(select * from security_schedule as b where b.pin = a.pin ) order by a.first_name", conn);
                using (var reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            id_user = reader["pin"].ToString(),
                            nama = reader["alias"].ToString()
                        });
                    }
                }
            }
            
            return list;
        }

        public List<Absensi> GetSecurity()
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select a.pin, a.alias from emp as a inner join func as c on a.func_id_auto = c.func_id_auto where c.func_name = 'Security' and a.emp_status = 0 and not exists(select * from security_schedule as b where b.pin = a.pin ) order by a.first_name", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            id_user = reader["pin"].ToString(),
                            nama = reader["alias"].ToString()
                        });
                    }
                }
            }
            return list;
        }

        public List<Absensi> EditSchedule(string pin)
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(String.Format("select a.start_date, a.pin, b.alias from security_schedule as a " +
                    "inner join emp as b on a.pin = b.pin " +
                    "where a.pin = '{0}'", pin), conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            id_user = reader["pin"].ToString(),
                            nama = reader["alias"].ToString(),
                            start_date = Convert.ToDateTime(reader["start_date"].ToString())
                        });
                    }
                }
            }

            return list;
        }

        public List<Absensi> UpdateSchedule(string pin, string start_date)
        {
            List<Absensi> list = new List<Absensi>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd1 = new MySqlCommand(String.Format("update security_schedule set start_date = '{0}' where pin = '{1}'", start_date, pin), conn);
                using(var reader1 = cmd1.ExecuteReader())
                {
                    while (reader1.Read())
                    {
                    }
                }
                conn.Close();
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(String.Format("select a.pin , a.start_date, b.alias from security_schedule as a " +
                    "inner join emp as b on a.pin = b.pin " +
                    "where a.pin = '{0}'", pin), conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Absensi()
                        {
                            id_user = reader["pin"].ToString(),
                            nama = reader["alias"].ToString(),
                            start_date = Convert.ToDateTime(reader["start_date"].ToString())
                        });
                    }
                }
            }

            return list;
        }
    }
}
