﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebExample.Models
{
    public class Content
    {
        //artikel models

        [Display(Name = "Foto")]
        public string foto { get; set; }

        [Display(Name = "Judul Artikel")]
        public string judul { get; set; }

        [Display(Name = "Artikel")]
        public string artikel { get; set; }

        [Display(Name = "Tanggal")]
        public DateTime tgl_upload { get; set; }

        [Display(Name = "Penulis")]
        public string penulis { get; set; }
    }
}
