﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Npgsql;

namespace WebExample.Models
{
    public class ContentContext
    {
        public string ConnectionString { get; set; }

        public ContentContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(ConnectionString);
        }

        public List<Content> ListArtikel()
        {
            List<Content> list = new List<Content>();
            using (NpgsqlConnection conn = GetConnection())
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("select * from artikel order by tanggal desc",conn);
                using(var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Content()
                        {
                            foto = reader["foto"].ToString(),
                            judul = reader["judul"].ToString(),
                            artikel = reader["artikel"].ToString(),
                            tgl_upload = Convert.ToDateTime(reader["tanggal"].ToString()),
                            penulis = reader["username"].ToString()
                        });
                    }
                }
            }
            return list;
        }

        public List<Content> SlideArtikel()
        {
            List<Content> list = new List<Content>();
            using (NpgsqlConnection conn = GetConnection())
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("select * from artikel order by tanggal desc limit 0,5", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Content()
                        {
                            foto = reader["foto"].ToString(),
                            judul = reader["judul"].ToString(),
                            artikel = reader["artikel"].ToString(),
                            tgl_upload = Convert.ToDateTime(reader["tanggal"].ToString()),
                            penulis = reader["username"].ToString()
                        });
                    }
                }
            }
            return list;
        }
    }
}
