﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCore.Identity.PostgreSQL.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebExample.Models;
using WebExample.Services;
using AspNetCore.Identity.PostgreSQL.Stores;
using IdentityRole = AspNetCore.Identity.PostgreSQL.IdentityRole;
namespace WebExample
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            //Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddUserStore<UserStore<ApplicationUser>> ()
                .AddRoleStore<RoleStore<IdentityRole>> ()
                .AddRoleManager<RoleManager<IdentityRole>>()
                    .AddDefaultTokenProviders();

            // Add application services.

            services.AddTransient<IEmailSender, EmailSender>();
            IdentityDbConfig.StringConnectionName= "DefaultCon";
            services.AddMvc();
            services.AddSingleton(_ => Configuration);
            services.Add(new ServiceDescriptor(typeof(AbsensiContext), new AbsensiContext(Configuration.GetConnectionString("DefaultConnection"))));
            services.Add(new ServiceDescriptor(typeof(ContentContext), new ContentContext(Configuration.GetConnectionString("ContentCon"))));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
