﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebExample.Models;
using Microsoft.AspNetCore.Identity;

namespace WebExample.Controllers
{ 

    [Authorize]
    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _SignInManager;
        private readonly UserManager<ApplicationUser> _UserManager;

        public HomeController(
             UserManager<ApplicationUser> userManager,
             SignInManager<ApplicationUser> signInManager)
        {
            _UserManager = userManager;
            _SignInManager = signInManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            if (_SignInManager.IsSignedIn(User))
            {
                AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
                var user = await _UserManager.GetUserAsync(User);
                return View(context.GetIdByEmail(user.Email.ToLower()));
            }
            else
            {
                return View();
            }
            
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
