﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebExample.Models;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebExample.Controllers
{
    public class AbsensiController : Controller
    {
        private readonly SignInManager<ApplicationUser> _SignInManager;
        private readonly UserManager<ApplicationUser> _UserManager;

        public AbsensiController(
             UserManager<ApplicationUser> userManager,
             SignInManager<ApplicationUser> signInManager)
        {
            _UserManager = userManager;
            _SignInManager = signInManager;
        }
        
        // GET: /<controller>/
        [Authorize]
        public async Task<IActionResult> Index(string str_search)
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else if (!String.IsNullOrEmpty(str_search))
            {
                return View(context.GetDetails().Where(x => x.nama.Contains(str_search)).ToList());
            }
            else
            {
                return View(context.GetDetails());
            }
        }

        public async Task<IActionResult> Details(string ID, string start_date, string end_date, string this_week, string this_month)
        {
            var user = await _UserManager.GetUserAsync(User);
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            string current_user = context.GetPinByEmail(user.Email.ToString().ToLower());
            if (user.UserName != "nabilaap" && current_user != ID)
            {
                return RedirectToAction("Details", "Absensi", new { ID = current_user });
            }
            else if (!String.IsNullOrEmpty(start_date) && !String.IsNullOrEmpty(end_date))
            {
                return View(context.GetDetailAbsen(ID).Where(x => x.tanggal >= Convert.ToDateTime(start_date) && x.tanggal <= Convert.ToDateTime(end_date)));
            }
            else
            {
                if (!String.IsNullOrEmpty(this_month))
                {
                    return View(context.GetDetailAbsen(ID).Where(x => x.tanggal >= DateTime.Now.AddMonths(-1) && x.tanggal <= DateTime.Now));
                }
                else
                {
                    if (!String.IsNullOrEmpty(this_week))
                    {
                        return View(context.GetDetailAbsen(ID).Where(x => x.tanggal >= DateTime.Now.AddDays(-7) && x.tanggal <= DateTime.Now));
                    }
                    else
                    {
                        return View(context.GetDetailAbsen(ID).Where(x => x.tanggal >= DateTime.Now.AddMonths(-1) && x.tanggal <= DateTime.Now));
                    }
                }
            }
        }

        public async Task<IActionResult> ReportDetails(string ID, string start_date, string end_date)
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            Response.Headers.Clear();
            Response.Headers.Add("content-disposition", "attachment;filename = Report_Rekapitulasi_Absensi.xls");
            Response.Headers.Add("content-type", "application/vnd.ms-excel");
            var user = await _UserManager.GetUserAsync(User);
            if(user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else if (!String.IsNullOrEmpty(start_date) && !String.IsNullOrEmpty(end_date))
            {
                return View(context.GetReportDetail(ID, start_date, end_date));
            }
            else
            {
                return View(context.GetDetailAbsen(ID));
            }
        }

        public async Task<IActionResult> TodayAbsen()
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else
            {
                return View(context.GetTodayAbsen());
            }
        }

        public async Task<IActionResult> TodayAbsent()
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else
            {
                return View(context.GetTodayAbsent());
            }
        }


        public async Task<IActionResult> Rekap(string start_date, string end_date)
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else
            {
                return View(context.GetRekapPeriode(start_date, end_date));
            }
        }

        public IActionResult ReportRekap(string start_date, string end_date)
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            Response.Headers.Clear();
            Response.Headers.Add("content-disposition", "attachment;filename = Report_Rekapitulasi_Absensi.xls");
            Response.Headers.Add("content-type", "application/vnd.ms-excel");
            return View(context.GetRekapPeriode(start_date, end_date));
        }

        public async Task<IActionResult> Schedule()
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else
            {
                return View(context.GetSchedule());
            }
        }
        
        public async Task<IActionResult> AddSchedule(string pin, string start_date)
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else if (!String.IsNullOrEmpty(pin) && !String.IsNullOrEmpty(start_date))
            {
                return View(context.CreateSchedule(pin, start_date));
            }
            else
            {
                return View(context.GetSecurity());
            }
        }

        public async Task<IActionResult> EditSchedule(string ID, string start_date)
        {
            AbsensiContext context = HttpContext.RequestServices.GetService(typeof(AbsensiContext)) as AbsensiContext;
            var user = await _UserManager.GetUserAsync(User);
            if (user.UserName != "nabilaap")
            {
                return RedirectToAction("Details", "Absensi", new { ID = context.GetPinByEmail(user.Email.ToString().ToLower()) });
            }
            else if (!String.IsNullOrEmpty(start_date))
            {
                return View(context.UpdateSchedule(ID, start_date));
            }
            else
            {
                return View(context.EditSchedule(ID));
            }
        }
    }
}
